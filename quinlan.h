#include <iostream>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
using namespace std;

typedef pair<string, vector<string> > sample_type;
typedef vector<sample_type> samples_type;
typedef map<string, double> dictionary_type;
typedef pair<string, double> dictionary_item_type;
typedef vector<dictionary_type> dictionaries_type; 

class Quinlan {
    // ime node-a v drevesu - za izpis grafa
    string node_name;
    // razred v katerega se razvrstijo vzorci na tem delu drevesa
    string class_name;
    // pove ali je na listu drevesa
    bool leaf = false;
    // indeks atributa s katerim se odloca na tem delu drevesa
    int selected_attribute;
    // verjetnost, da dolocenemu vzorcu pripada vrednost atributa
    dictionary_type attribute_frequency;
    // otroci drevesa, ki sovpadajo vrednostim atributa v attribute_frequency
    vector<Quinlan> children;
public:
    // konstruktor - tu se izdela celotno drevo
    Quinlan (samples_type::iterator start, samples_type::iterator end, string new_name);
    // funkcija, klasificira vzorec
    string classify (sample_type& sample);
    // funkcija, ki vrne graf drevesa v dot notaciji
    void getGraph(stringstream& graph);
};
