#include "quinlan.h"
using namespace std;

#include <gflags/gflags.h>

DEFINE_string(filename, "../data/operative.data", "input file");

class parseDataLine {
public:
    sample_type operator() (string s) {
        vector<string> v;
        istringstream ss(s);
        while (ss) {
            string s;
            if (!getline(ss,s,',')) break;
            v.push_back(s);
        }
        string razred = *(--v.end());
        v.pop_back();
        return make_pair(razred, v);
    }
};

int main (int argc, char* argv[]) {
	gflags::ParseCommandLineFlags(&argc, &argv, true);

    fstream datafile (FLAGS_filename);
    double learn_ratio = 2./3;
    //int seed = 1427668796;
    int seed = time(0); 
    if (argc > 2) learn_ratio = stod(argv[2]);
    if (argc > 3) seed = stoi(argv[3]);

    // prebere datoteko z vzorci
    vector<string> lines; // vrstice iz datoteke
    copy( istream_iterator<string>(datafile), istream_iterator<string>(), back_inserter(lines) );
    samples_type samples; // vzorec iz vrstice
    transform( lines.begin(), lines.end(), back_inserter(samples), parseDataLine() );

    // nakljucni razpored
    srand(seed);
    random_shuffle(samples.begin(), samples.end());

    // postavi iterator na 2/3 vzorcev
    auto middle = samples.begin();
    advance(middle, distance(samples.begin(), samples.end())*learn_ratio);

    // ustvari quinlan drevo s prvima 2/3 vzorcev
    cout << "Vseh vzorcev: " << samples.size() << "; ucenje na " << distance(samples.begin(), middle) << " vzorcih." << endl;
    Quinlan qn(samples.begin(), middle, "top");

    // razvrsti zadnjo tretjino in presteje pravilno razvrscene
    int correct_count = 0;
    int total_count = distance(middle, samples.end());
    for_each(middle, samples.end(), [&qn, &correct_count] (pair<string, vector<string> >& p) {
        if (qn.classify(p) == p.first) ++correct_count;
    });
    cout << "Pravilno razvrsceni: " << correct_count << "/" << total_count << " (" << double (correct_count) * 100 / total_count << "%)" << endl;

    // ustvari graf v dot formatu in klice program dot
    stringstream graph;
    qn.getGraph(graph);
    ofstream graphfile("out/graph.gv");
    graphfile << "digraph G {" << endl;
    graphfile << graph.rdbuf();
    graphfile << "}" << endl;
    system("dot -Tpng out/graph.gv -o out/graph.png");
    cout << "Graf izrisan v datoteki graph.png ." << endl;

    return 0;
}
