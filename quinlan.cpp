#include <map>
#include <functional>
#include "quinlan.h"

struct compareclass_frequency : public binary_function<sample_type, sample_type, bool> {
    bool operator() (const sample_type& s1, const sample_type& s2) const {
        return s1.first == s2.first;
    }
};

struct compareattribute_frequencies : public binary_function<sample_type, sample_type, bool> {
    bool operator() (const sample_type& s1, const sample_type& s2) const {
        return s1.second == s2.second;
    }
};

Quinlan::Quinlan (samples_type::iterator begin, samples_type::iterator end, string new_name) {
    node_name = new_name;
    // ce vsi vzorci pripadajo istemu razredu, zakljuci drevo
    if (adjacent_find(begin, end, not2(compareclass_frequency())) == end) {
        leaf = true;
        class_name = (*begin).first;
        return;
    }
    // ce imajo vsi vzorci enake atribute, vendar pripadajo razlicnim razredom, zakljuci drevo z najpogostejsim razredom 
    if (adjacent_find(begin, end, not2(compareattribute_frequencies())) == end) {
        leaf = true;
        int max_count = 0;
        string name;
        dictionary_type classnames;
        for_each(begin, end, [&max_count, &classnames, &name] (sample_type& sample) {
            classnames[sample.first] += 1.0;
            if (classnames[sample.first] > max_count) {
                max_count = classnames[sample.first];
                name = sample.first;
            }
        });
        class_name = name;
        return;
    }

    // za vsak atribut poisce mnozice vrednosti in pripadajoce verjetnosti
    dictionaries_type attribute_frequencies ((*begin).second.size(), dictionary_type());
    for_each(begin, end, [&attribute_frequencies] (sample_type sample) {
        for (int i = 0; i < attribute_frequencies.size(); ++i) {
            // ignorira vzorec, za katerega vrednost atributa ni znana
            if (sample.second[i] == "?") continue;
            attribute_frequencies[i][sample.second[i]] += 1.0;
        }
    });
    for (auto di = attribute_frequencies.begin(); di != attribute_frequencies.end(); ++di) {
        double sum = 0.0;
        for_each((*di).begin(), (*di).end(), [&sum](pair<const string, double>& p) {
            sum += p.second;
        });
        for_each((*di).begin(), (*di).end(), [&sum](pair<const string, double>& p) {
            p.second /= sum;
        });
    }

    // izracuna entropije za vsak atribut in poisce atribut z najmanjso entropijo
    double min_entropy = -1.;
    for (int i = 0; i < attribute_frequencies.size(); ++i) {
        // ce je v mnozici vzorcev le ena vrednost atributa, z njim ni mogoce ustvariti drevesa
        if (attribute_frequencies[i].size() == 1) continue;

        double entropy = 0.0;
        for (auto attribute_probability = attribute_frequencies[i].begin(); attribute_probability != attribute_frequencies[i].end(); ++attribute_probability) {
            double class_entropy = 0.0;
            dictionary_type class_frequency;
            for (auto sample = begin; sample != end; ++sample) {
                auto attribute = (*sample).second.begin();
                advance(attribute, i);
                if ((*attribute) == (*attribute_probability).first) {
                    class_frequency[(*sample).first] += 1.0;
                }
            }
            double class_sum = 0.0;
            for_each(class_frequency.begin(), class_frequency.end(), [&class_sum](const dictionary_item_type& class_count) {
                class_sum += class_count.second;
            });
            for_each(class_frequency.begin(), class_frequency.end(), [&class_sum, &class_entropy](const dictionary_item_type& class_count) {
                double class_probability = class_count.second / class_sum;
                if (class_probability != 0)
                    class_entropy += class_probability * log2(class_probability);
            });
            entropy -= (*attribute_probability).second * class_entropy;
        }

        // iskanje minimalne vrednosti
        if ((min_entropy == -1) || (entropy < min_entropy)) {
            min_entropy = entropy;
            selected_attribute = i;
        }
    }

    // razdeli vzorce glede na izbrani atribut in jih dodeli otrokom v drevesu
    auto attribute = attribute_frequencies.begin();
    advance(attribute, selected_attribute);
    attribute_frequency = (*attribute);
    stringstream ss;
    ss << selected_attribute;
    string child_name = node_name;
    for (auto attribute_value = attribute_frequency.begin(); attribute_value != attribute_frequency.end(); ++attribute_value) {
        samples_type child_samples;
        for (auto sample = begin; sample != end; ++sample) {
            auto sample_attribute = (*sample).second.begin();
            advance(sample_attribute, selected_attribute);
            if (((*sample_attribute) == (*attribute_value).first) || ((*sample_attribute) == "?")) { // TODO: partition namesto copy
                child_samples.push_back(*sample);
            }
        }
        child_name += ss.str();
        children.push_back( Quinlan(child_samples.begin(), child_samples.end(), child_name) );
    }
}

struct mapCompare {
    bool operator() (dictionary_item_type c1, dictionary_item_type c2) {
        return c1.second < c2.second;
    }
};

string Quinlan::classify (sample_type& sample) {
    if (leaf) {
        // ce je list, vrne klasificirano vrednost
        return class_name;
    }

    string sample_attribute = sample.second[selected_attribute];
    
    // poisce vrednost atributa in klice pripadajocega otroka
    auto child = children.begin();
    for (auto attribute = attribute_frequency.begin(); attribute != attribute_frequency.end(); ++attribute) {
        if ((*attribute).first == sample_attribute) {
            return (*child).classify(sample);
        }
        ++child;
    }

    // ce vrednosti atributa ni v poddrevesu, potem klice vse otroke in se odloci glede verjetnost razredov iz ucne mnozice
    dictionary_type class_frequency;
    child = children.begin();
    for (auto attribute = attribute_frequency.begin(); attribute != attribute_frequency.end(); ++attribute) {
        string classified = (*child).classify(sample);
        class_frequency[classified] += (*attribute).second;
        ++child;
    }
    auto best_class = max_element(class_frequency.begin(), class_frequency.end(), mapCompare());
    return (*best_class).first;
}

void Quinlan::getGraph(stringstream& graph) {
    if (leaf) {
        // ce je list grafu pripise klasificiran razred in postavi obliko na pravokotnik
        graph << node_name << " [label=\"" << class_name << "\", shape=box];" << endl;
        return;
    }
    
    // ce ni list pripise atribut po katerem odloca
    graph << node_name << " [label=\"x" << selected_attribute << "\"];" << endl;
    
    // v graf zapise vse povezave do otrok in jih oznaci z vrednostjo atributa
    auto child = children.begin();
    for (auto attribute = attribute_frequency.begin(); attribute != attribute_frequency.end(); ++attribute) {
        graph << node_name << " -> " << (*child).node_name << "[label=\"" << (*attribute).first << "\"];" << endl;

        (*child).getGraph(graph);
        ++child;
    }
}
